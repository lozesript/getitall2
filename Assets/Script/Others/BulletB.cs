﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletB : MonoBehaviour
{
    public float speed = 6.0f;
    public float destroytime = 5;
    private GameObject enemy;
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody>().AddForce(this.transform.forward * speed);
    }

    // Update is called once per frame
    void Update()
    {
        destroytime -= Time.deltaTime * 1;
        if (destroytime <= 0)
        {
            Destroy(gameObject);
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Enemy")
        {
            collision.gameObject.GetComponent<GuardAI>().Sleep();
            Destroy(gameObject);
        }
    }
}
