﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 6.0f;
    public float destroytime = 5;
    private bool isIn = false;
    private GameObject enemy;
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody>().AddForce(this.transform.forward * speed);
    }

    // Update is called once per frame
    void Update()
    {
        destroytime -= Time.deltaTime * 1;
        if (destroytime <= 0)
        {
            if(isIn)
            {
                enemy.GetComponent<GuardAI>().ChangeBack();
            }
            Destroy(gameObject);
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Enemy")
        {
            other.GetComponent<GuardAI>().ChangeStage(this.transform);
            isIn = true;
            enemy = other.gameObject;
        }
    }
}
