﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Controller : MonoBehaviour
{

    public float moveSpeed = 6f;
    public float turnSmoothing = 15f;
    public float speedDampTime = 0.1f;
    public GameObject bullet;
    public GameObject bulletB;
    public Transform fireSpot;

    Rigidbody myRigidbody;
    //Camera viewCamera;
    Vector3 velocity;
    private float vertical;
    private float horizontal;
    //private LastPlayerSighting lastPlayerSighting;
    // private SphereCollider col;
    private float delay;
    private float cooldown;

    void Awake()
    {
        delay = 0;
        Time.timeScale = 1;
        myRigidbody = GetComponent<Rigidbody>();
        //viewCamera = Camera.main;
    }

    void Update()
    {
        if (delay > 0)
        { delay -= Time.deltaTime * 1; }
        if (cooldown > 0)
        { cooldown -= Time.deltaTime * 1; }
        if (Input.GetMouseButton(0))
        {
            if (cooldown <= 0)
            {
                Instantiate(bullet, fireSpot.position, this.transform.rotation);
                cooldown = 2f;
            }
        }
        if (Input.GetMouseButton(1))
        {
            if (cooldown <= 0)
            {
                Instantiate(bulletB, fireSpot.position, this.transform.rotation);
                cooldown = 2f;
            }
        }
        Ray cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
        float rayLenght;

        if(groundPlane.Raycast(cameraRay, out rayLenght))
        {
            Vector3 pointTolook = cameraRay.GetPoint(rayLenght);
            Debug.DrawLine(cameraRay.origin, pointTolook, Color.blue);

            transform.LookAt(new Vector3(pointTolook.x,transform.position.y,pointTolook.z));
        }
    }

    void FixedUpdate()
    {
        vertical = Input.GetAxis("Vertical");
        horizontal = Input.GetAxis("Horizontal");
        MovementManager(horizontal, vertical);
    }
    void MovementManager(float horizontal, float vertical)
    {
       // Rotating(vertical, horizontal);
        if (horizontal != 0f || vertical != 0f)
        {
            myRigidbody.velocity = new Vector3((horizontal) * moveSpeed * Time.fixedDeltaTime, 0f, vertical * moveSpeed * Time.fixedDeltaTime);
        }

    }

    void Rotating()
    {
        Vector3 targetDirection = Camera.main.WorldToScreenPoint(Input.mousePosition);
        Quaternion targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);
        Quaternion newRotation = Quaternion.Lerp(myRigidbody.rotation, targetRotation, turnSmoothing * Time.deltaTime);
        newRotation.x = -90;
        myRigidbody.MoveRotation(newRotation);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Guard" )
        {
            //
        }
    }

}
