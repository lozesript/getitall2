﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class GuardAI : MonoBehaviour
{
    NavMeshAgent agent;
    [SerializeField] float decisionDelay = 1f;
    [SerializeField] Transform playerToChase;
    public Transform objectToChase;
    [SerializeField] Transform[] waypoints;
    int currentWaypoint = 0;
    bool isChaseObject = false;
    bool awake = true;

    public float detectPlayer = 3.0f;
    public float detectWapoint = 0.8f;
    public FieldofView enemyFoV;

    public bool isAlerted = false;


    enum EnemyStates
    {
        Patrolling,
        ChasingP,
        ChasingO,
        Sleep
    }

    [SerializeField] EnemyStates currentState;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        InvokeRepeating("SetDestination", 0.5f, decisionDelay);
        if (currentState == EnemyStates.Patrolling)
            agent.SetDestination(waypoints[currentWaypoint].position);
        enemyFoV = GetComponent<FieldofView>();

        isAlerted = false;
    }

    void Update()
    {

        if(awake)
        {
            if (currentState == EnemyStates.Patrolling)
            {
                if (Vector3.Distance(transform.position, waypoints[currentWaypoint].position) <= detectWapoint) //Enemy Detect Waypoint
                {
                    currentWaypoint++;
                    if (currentWaypoint == waypoints.Length)
                    {
                        currentWaypoint = 0;
                    }
                }
                agent.SetDestination(waypoints[currentWaypoint].position);
            }
        }
    }

    public void SetChasingPlayer(bool setChase)
    {
        if (setChase)
        {
            isAlerted = true;
            currentState = EnemyStates.ChasingP;
            //detectPlayer = enemyFoV.viewRadius; //After Detect
            agent.speed = 4.5f;
        }
        else if (isAlerted)
        {
            // When player exit the line of sight (BUT chased player already ALERTED)
            currentState = EnemyStates.Patrolling;
            detectPlayer = enemyFoV.viewRadius;
            agent.speed = 4;
        }
        else
        {
            // Don't see player yet
            currentState = EnemyStates.Patrolling;
            detectPlayer = enemyFoV.viewRadius;
            agent.speed = 3.5f;
        }


    }




    void SetDestination()
    {
        if (currentState == EnemyStates.ChasingO)
            agent.SetDestination(objectToChase.position);
        else if (currentState == EnemyStates.ChasingP)
            agent.SetDestination(playerToChase.position);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.name == "Player"&&awake)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    //private void OnDrawGizmos()
    // {
    //    Gizmos.DrawWireSphere(transform.position, radiusdetectPlayer);
    //}

    public void ChangeStage(Transform position)
    {
        if(awake)
        {
            currentState = EnemyStates.ChasingO;
            objectToChase = position;
            isChaseObject = true;
        }
        
    }
    public void ChangeBack()
    {
        if(awake)
        {
            currentState = EnemyStates.Patrolling;
            isChaseObject = false;
            objectToChase = null;
        }
        
    }
    public void Sleep()
    {
        currentState = EnemyStates.Sleep;
        awake = false;
    }
    


}